const cron = require("node-cron");
const conn = require("../config/db.js");
const {
  sendEmailNotification,
  sendLineNotification,
  sendSMSNotification,
  sendAppNotification,
} = require("../routes/notifunction.js");

//set date option for send notify
const options = { year: "numeric", month: "long", day: "numeric" };

//send notify function
async function sendNotification(
  code,
  line_type,
  pos_type,
  roundName,
  startDate,
  enddate,
  warnning,
  lastwarning
) {
  switch (line_type) {
    case "01":
      target_sql = `select distinct C.EMP_CODE ,C.PHONE_NO, C.EMAIL , C.LINE_TOKEN,DA.TOKEN from (
        select  
                 n1.* 
                 , case when n1.FORM_CONFIG = '01' then (select CONFIRM_STATUS as from FORM_TXN_SECTION1_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}') AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') 
                  when n1.FORM_CONFIG = '02' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION2_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') 
                  when n1.FORM_CONFIG = '03' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION3_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') 
                  when n1.FORM_CONFIG = '04' then (select CONFIRM_STATUS as ss from FORM_TXN_RESEARCH_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y')
                   when n1.FORM_CONFIG = '05' then (select CONFIRM_STATUS as ss from FORM_TXN_SUMMARY_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') end NOF
        from (
        select t2.* , c1.EMP_CODE from (
        select t1.* , B.FORM_NAME , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONTENT_TYPE' and F.alph_code = t1.FORM_CONTENT_TYPE) FORM_CONTENT_TYPE_NAME from  (
        select distinct FORM_CONFIG , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONFIG' and F.alph_code = A.FORM_CONFIG) FORM_CONFIG_NAME
        , FORM_CODE ,  (select FORM_CONTENT_TYPE from FORM_MAS B where A.FORM_CODE = B.FORM_CODE) FORM_CONTENT_TYPE
        , FORM_GROUP ,  LINE_TYPE , POS_TYPE from FORM_MAS_MAPPING A where (LINE_TYPE , POS_TYPE , FORM_GROUP 
         ) in (
        select LINE_TYPE , POS_TYPE , FORM_GROUP from EMP_MAS where LINE_TYPE = '01' AND POS_TYPE = '${pos_type}'
        AND EMP_CODE is not null ) 
        order by LINE_TYPE , POS_TYPE , FORM_GROUP , FORM_CONFIG ) t1
        inner join FORM_MAS B on t1.FORM_CODE = B.FORM_CODE ) t2 
        inner join EMP_MAS c1 on t2.LINE_TYPE = c1.LINE_TYPE and t2.POS_TYPE = c1.POS_TYPE and t2.FORM_GROUP = c1.FORM_GROUP 
         ) n1 ) n2 
        inner join EMP_MAS C on n2.EMP_CODE = C.EMP_CODE and n2.LINE_TYPE = c.LINE_TYPE and n2.POS_TYPE = c.POS_TYPE and n2.FORM_GROUP = c.FORM_GROUP 
        left join DEVICE_APP da on C.EMP_CODE = DA.EMP_CODE
        where NOF is null`;
      break;

    //case 02  use this sql no form_group pairing
    case "02":
      target_sql = `select distinct C.EMP_CODE ,C.PHONE_NO, C.EMAIL , C.LINE_TOKEN,DA.TOKEN from (
        select  
                 n1.* 
                 , case when n1.SECTION_NO = '2'AND FORM_CONFIG = '07' then (select CONFIRM_STATUS as from FORM_TXN_AGREEMENT_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}') AND CONFIRM_STATUS = 'Y') 
                  when n1.SECTION_NO = '3' AND FORM_CONFIG = '07' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION2_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') 
                  when n1.SECTION_NO = '4'AND FORM_CONFIG = '07' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION3_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') 
                   when n1.FORM_CONFIG = '05' then (select CONFIRM_STATUS as ss from FORM_TXN_SUMMARY_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')AND CONFIRM_STATUS = 'Y'AND APPROVE_STATUS = 'Y') end NOF
        from (
        select t2.* , c1.EMP_CODE from (
        select t1.* , B.FORM_NAME , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONTENT_TYPE' and F.alph_code = t1.FORM_CONTENT_TYPE) FORM_CONTENT_TYPE_NAME from  (
        select distinct FORM_CONFIG , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONFIG' and F.alph_code = A.FORM_CONFIG) FORM_CONFIG_NAME
        , FORM_CODE ,  (select FORM_CONTENT_TYPE from FORM_MAS B where A.FORM_CODE = B.FORM_CODE) FORM_CONTENT_TYPE
         ,SECTION_NO,  LINE_TYPE , POS_TYPE from FORM_MAS_MAPPING A where (LINE_TYPE , POS_TYPE 
         ) in (
        select LINE_TYPE , POS_TYPE from EMP_MAS where LINE_TYPE = '02' AND POS_TYPE = '${pos_type}'
        AND EMP_CODE is not null ) 
        order by LINE_TYPE , POS_TYPE , FORM_CONFIG ) t1
        inner join FORM_MAS B on t1.FORM_CODE = B.FORM_CODE ) t2 
        inner join EMP_MAS c1 on t2.LINE_TYPE = c1.LINE_TYPE and t2.POS_TYPE = c1.POS_TYPE 
         ) n1 ) n2 
        inner join EMP_MAS C on n2.EMP_CODE = C.EMP_CODE and n2.LINE_TYPE = c.LINE_TYPE and n2.POS_TYPE = c.POS_TYPE 
        left join DEVICE_APP DA on C.EMP_CODE = DA.EMP_CODE
        where NOF is null `;
      break;
  }

  let targetresult = await conn.ociExecute(target_sql, "", "");
  if (targetresult.results.lenght <= 0) {
    res.status(200).json({ message: "No data found." });
  }
  const mailtarget = targetresult.results.map((row) => row.EMAIL);
  const emailAddress = mailtarget
    .filter((item) => item !== null)
    .find((item) => typeof item === "string");
  const linetoken = targetresult.results.map((row) => row.LINE_TOKEN);
  const LineAddress = linetoken
    .filter((value) => value !== null)
    .map((value) => value);
  const SMStarget = targetresult.results.map((row) => row.EMAIL);
  const SMSAddress = SMStarget.filter((item) => item !== null).find(
    (item) => typeof item === "string"
  );
  const fcmtarget = targetresult.results.map((row) => row.TOKEN);
  const FCMAddress = fcmtarget
    .filter((value) => value !== null)
    .map((value) => value);

  console.log(emailAddress);
  console.log(LineAddress);

  //check date to send notification
  const [day, month, year] = enddate.split("/");
  const endDate = new Date(year, month - 1, day);

  // Calculate warning date (first warning days before end date)
  const warningDate = new Date(
    endDate.getTime() - warnning * 24 * 60 * 60 * 1000
  );

  // Calculate last warning date (last time days before end date)
  const lastWarningDate = new Date(
    endDate.getTime() - lastwarning * 24 * 60 * 60 * 1000
  );
  //format end date
  const formattedDate = endDate.toLocaleDateString("th-TH", options);

  // Check if warning date is the same as today
  const today = new Date();
  console.log(lastWarningDate, today);

  if (today >= warningDate && today <= lastWarningDate) {
    const mailsubject = `แจ้งเตือน ${roundName}`;
    const message = `แจ้งเตือน ${roundName} \nกำลังใกล้สิ้นสุดเวลาประเมิน กรุณาเข้าไปทำแบบประเมินให้ครบถ้วน  \nเหลือระยะเวลา ${warnning} วัน! \nสิ้นสุดวันที่ ${formattedDate}`;
    sendEmailNotification(
      emailAddress,
      (type = null),
      (subject = null),
      (desc = null),
      pos_type,
      line_type,
      mailsubject,
      message
    );

    sendLineNotification(
      LineAddress,
      (type = null),
      mailsubject,
      message,
      line_type,
      pos_type
    );
    sendSMSNotification(
      SMSAddress,
      message,
      subject,
      desc,
      line_type,
      pos_type
    );
    sendAppNotification(FCMAddress, subject, desc, line_type, pos_type);
  } else if (today >= lastWarningDate) {
    const mailsubject = `แจ้งเตือน ${roundName}`;
    const message = `แจ้งเตือน ${roundName} \nกำลังใกล้สิ้นสุดเวลาประเมิน กรุณาเข้าไปทำแบบประเมินให้ครบถ้วน  \nเหลือระยะเวลา ${lastwarning} วัน! \nสิ้นสุดวันที่ ${formattedDate}`;
    console.log("sendmail");
    sendEmailNotification(
      emailAddress,
      (type = null),
      (subject = null),
      (desc = null),
      pos_type,
      line_type,
      mailsubject,
      message
    );
    sendLineNotification(
      LineAddress,
      (type = null),
      mailsubject,
      message,
      line_type,
      pos_type
    );
    sendSMSNotification(
      SMSAddress,
      message,
      subject,
      desc,
      line_type,
      pos_type
    );
    sendAppNotification(FCMAddress, subject, desc, line_type, pos_type);
  }
}

async function schedulenotify() {
  //sql to check curr round
  curr_round_sql = `SELECT DISTINCT A.RND_CURR_SETUP_CODE, A.LINE_TYPE, A.POS_TYPE,RM.ROUND_NAME , C.ROUND_FROM_DATE,C.ROUND_END_DATE,C.WARNING_DAYS,C.LAST_WARNING_DAYS
  FROM ROUND_POSTYPE A 
  INNER JOIN ROUND_CURRENT_SETUP B 
  ON A.RND_CURR_SETUP_CODE = B.RND_CURR_SETUP_CODE 
  JOIN ROUND_TXN C
  ON B.RND_TXN_NO = C.RND_TXN_NO 
  JOIN FORM_MAS_MAPPING D
  ON A.LINE_TYPE = D.LINE_TYPE AND A.POS_TYPE = D.POS_TYPE
  JOIN ROUND_MAS rm
  ON C.ROUND_NO = RM.ROUND_NO
  WHERE  B.OPEN_STATUS_YN = 'Y'
  `;
  let current_setup = await conn.ociExecute(curr_round_sql, "", "");
  console.log("run schdule event");

  //schedule the event with curr code , line_type and pos_type database from current round that open
  current_setup.results.forEach((event, index) => {
    cron.schedule(
      "01 14 * * *", //set send time hear "min hr * * *"
      () => {
        sendNotification(
          event.RND_CURR_SETUP_CODE,
          event.LINE_TYPE,
          event.POS_TYPE,
          event.ROUND_NAME,
          event.ROUND_FROM_DATE,
          event.ROUND_END_DATE,
          event.WARNING_DAYS,
          event.LAST_WARNING_DAYS
        );
      },
      {
        scheduled: true,
        timezone: "Asia/Bangkok",
      }
    );
  });
}

module.exports = {
  schedulenotify: schedulenotify,
  sendNotification: sendNotification,
};
