const cron = require("node-cron");
const conn = require("../config/db.js");
const {
  sendEmailNotification,
  sendLineNotification,
  sendSMSNotification,
  sendAppNotification,
} = require("../routes/notifunction.js");

//set date option for send notify
const options = { year: "numeric", month: "long", day: "numeric" };

//main function to select option
async function optionnotify(option) {
  switch (option) {
    case 1:
      //sql to check curr round
      curr_round_sql = `SELECT DISTINCT A.RND_CURR_SETUP_CODE, A.LINE_TYPE, A.POS_TYPE,RM.ROUND_NAME ,C.FROM_DATE,C.END_DATE, C.ROUND_FROM_DATE,C.ROUND_END_DATE,C.WARNING_DAYS,C.LAST_WARNING_DAYS
      FROM ROUND_POSTYPE A 
      INNER JOIN ROUND_CURRENT_SETUP B 
      ON A.RND_CURR_SETUP_CODE = B.RND_CURR_SETUP_CODE 
      JOIN ROUND_TXN C
      ON B.RND_TXN_NO = C.RND_TXN_NO 
      JOIN FORM_MAS_MAPPING D
      ON A.LINE_TYPE = D.LINE_TYPE AND A.POS_TYPE = D.POS_TYPE
      JOIN ROUND_MAS rm
      ON C.ROUND_NO = RM.ROUND_NO
      WHERE  B.OPEN_STATUS_YN = 'Y' AND B.STATUS = 'A'
      `;
      break;
    case 2:
      //sql to check curr round
      curr_round_sql = `SELECT DISTINCT A.RND_CURR_SETUP_CODE, A.LINE_TYPE, A.POS_TYPE,RM.ROUND_NAME ,C.FROM_DATE,C.END_DATE, C.ROUND_FROM_DATE,C.ROUND_END_DATE,C.WARNING_DAYS,C.LAST_WARNING_DAYS
      FROM ROUND_POSTYPE A 
      INNER JOIN ROUND_CURRENT_SETUP B 
      ON A.RND_CURR_SETUP_CODE = B.RND_CURR_SETUP_CODE 
      JOIN ROUND_TXN C
      ON B.RND_TXN_NO = C.RND_TXN_NO 
      JOIN FORM_MAS_MAPPING D
      ON A.LINE_TYPE = D.LINE_TYPE AND A.POS_TYPE = D.POS_TYPE
      JOIN ROUND_MAS rm
      ON C.ROUND_NO = RM.ROUND_NO 
      WHERE  B.OPEN_STATUS_YN = 'Y' AND B.CLOSE_STATUS_YN = 'Y' AND B.STATUS = 'A'
      `;
      break;
    case 3:
      //sql to check curr round
      curr_round_sql = `SELECT DISTINCT A.RND_CURR_SETUP_CODE, A.LINE_TYPE, A.POS_TYPE,RM.ROUND_NAME ,C.FROM_DATE,C.END_DATE, C.ROUND_FROM_DATE,C.ROUND_END_DATE,C.WARNING_DAYS,C.LAST_WARNING_DAYS
        FROM ROUND_POSTYPE A 
        INNER JOIN ROUND_CURRENT_SETUP B 
        ON A.RND_CURR_SETUP_CODE = B.RND_CURR_SETUP_CODE 
        JOIN ROUND_TXN C
        ON B.RND_TXN_NO = C.RND_TXN_NO 
        JOIN FORM_MAS_MAPPING D
        ON A.LINE_TYPE = D.LINE_TYPE AND A.POS_TYPE = D.POS_TYPE
        JOIN ROUND_MAS rm
        ON C.ROUND_NO = RM.ROUND_NO
        WHERE  B.OPEN_STATUS_YN = 'Y' 
        `;
      break;
    case 4:
      //sql to check curr round
      curr_round_sql = `SELECT DISTINCT A.RND_CURR_SETUP_CODE, A.LINE_TYPE, A.POS_TYPE,RM.ROUND_NAME,C.FROM_DATE,C.END_DATE C.ROUND_FROM_DATE,C.ROUND_END_DATE,C.WARNING_DAYS,C.LAST_WARNING_DAYS
      FROM ROUND_POSTYPE A 
      INNER JOIN ROUND_CURRENT_SETUP B 
      ON A.RND_CURR_SETUP_CODE = B.RND_CURR_SETUP_CODE 
      JOIN ROUND_TXN C
      ON B.RND_TXN_NO = C.RND_TXN_NO 
      JOIN FORM_MAS_MAPPING D
      ON A.LINE_TYPE = D.LINE_TYPE AND A.POS_TYPE = D.POS_TYPE
      JOIN ROUND_MAS rm
      ON C.ROUND_NO = RM.ROUND_NO
      WHERE  B.OPEN_STATUS_YN = 'Y'
      `;
      break;
  }
  let current_setup = await conn.ociExecute(curr_round_sql, "", "");
  console.log(current_setup);
  console.log("run schdule event");

  //schedule the event with curr code , line_type and pos_type database from current round that open
  current_setup.results.forEach((event, index) => {
    sendNotification(
      event.RND_CURR_SETUP_CODE,
      event.LINE_TYPE,
      event.POS_TYPE,
      event.ROUND_NAME,
      event.FROM_DATE,
      event.END_DATE,
      event.ROUND_FROM_DATE,
      event.ROUND_END_DATE,
      option
    );
  });
}

async function sendNotification(
  code,
  line_type,
  pos_type,
  roundName,
  fromDate,
  endDate,
  RfromDate,
  RendDate,
  option
) {
  switch (line_type) {
    case "01":
      target_sql = `select distinct C.EMP_CODE ,C.PHONE_NO, C.EMAIL , C.LINE_TOKEN,DA.TOKEN from (
            select  
                     n1.* 
                     , case when n1.FORM_CONFIG = '01' then (select CONFIRM_STATUS as from FORM_TXN_SECTION1_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) 
                      when n1.FORM_CONFIG = '02' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION2_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) 
                      when n1.FORM_CONFIG = '03' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION3_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) 
                      when n1.FORM_CONFIG = '04' then (select CONFIRM_STATUS as ss from FORM_TXN_RESEARCH_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}'))
                       when n1.FORM_CONFIG = '05' then (select CONFIRM_STATUS as ss from FORM_TXN_SUMMARY_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) end NOF
            from (
            select t2.* , c1.EMP_CODE from (
            select t1.* , B.FORM_NAME , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONTENT_TYPE' and F.alph_code = t1.FORM_CONTENT_TYPE) FORM_CONTENT_TYPE_NAME from  (
            select distinct FORM_CONFIG , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONFIG' and F.alph_code = A.FORM_CONFIG) FORM_CONFIG_NAME
            , FORM_CODE ,  (select FORM_CONTENT_TYPE from FORM_MAS B where A.FORM_CODE = B.FORM_CODE) FORM_CONTENT_TYPE
            , FORM_GROUP ,  LINE_TYPE , POS_TYPE from FORM_MAS_MAPPING A where (LINE_TYPE , POS_TYPE , FORM_GROUP 
             ) in (
            select LINE_TYPE , POS_TYPE , FORM_GROUP from EMP_MAS where LINE_TYPE = '01' AND POS_TYPE = '${pos_type}'
            AND EMP_CODE is not null ) 
            order by LINE_TYPE , POS_TYPE , FORM_GROUP , FORM_CONFIG ) t1
            inner join FORM_MAS B on t1.FORM_CODE = B.FORM_CODE ) t2 
            inner join EMP_MAS c1 on t2.LINE_TYPE = c1.LINE_TYPE and t2.POS_TYPE = c1.POS_TYPE and t2.FORM_GROUP = c1.FORM_GROUP 
             ) n1 ) n2 
            inner join EMP_MAS C on n2.EMP_CODE = C.EMP_CODE and n2.LINE_TYPE = c.LINE_TYPE and n2.POS_TYPE = c.POS_TYPE and n2.FORM_GROUP = c.FORM_GROUP 
            left join DEVICE_APP da on C.EMP_CODE = DA.EMP_CODE
            where NOF is null`;
      break;

    //case 02  use this sql no form_group pairing
    case "02":
      target_sql = `select distinct C.EMP_CODE ,C.PHONE_NO, C.EMAIL , C.LINE_TOKEN,DA.TOKEN from (
            select  
                     n1.* 
                     , case when n1.SECTION_NO = '2'AND FORM_CONFIG = '07' then (select CONFIRM_STATUS as from FORM_TXN_AGREEMENT_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) 
                      when n1.SECTION_NO = '3' AND FORM_CONFIG = '07' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION2_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) 
                      when n1.SECTION_NO = '4'AND FORM_CONFIG = '07' then (select CONFIRM_STATUS as ss from FORM_TXN_SECTION3_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) 
                       when n1.FORM_CONFIG = '05' then (select CONFIRM_STATUS as ss from FORM_TXN_SUMMARY_MAS A where n1.EMP_CODE = A.EMP_CODE AND n1.FORM_CODE = A.FORM_CODE AND A.RND_CURR_SETUP_CODE in ('${code}')) end NOF
            from (
            select t2.* , c1.EMP_CODE from (
            select t1.* , B.FORM_NAME , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONTENT_TYPE' and F.alph_code = t1.FORM_CONTENT_TYPE) FORM_CONTENT_TYPE_NAME from  (
            select distinct FORM_CONFIG , (select alph_val from ms_alpha F where F.set_type = 'FORM_CONFIG' and F.alph_code = A.FORM_CONFIG) FORM_CONFIG_NAME
            , FORM_CODE ,  (select FORM_CONTENT_TYPE from FORM_MAS B where A.FORM_CODE = B.FORM_CODE) FORM_CONTENT_TYPE
             ,SECTION_NO,  LINE_TYPE , POS_TYPE from FORM_MAS_MAPPING A where (LINE_TYPE , POS_TYPE 
             ) in (
            select LINE_TYPE , POS_TYPE from EMP_MAS where LINE_TYPE = '02' AND POS_TYPE = '${pos_type}'
            AND EMP_CODE is not null ) 
            order by LINE_TYPE , POS_TYPE , FORM_CONFIG ) t1
            inner join FORM_MAS B on t1.FORM_CODE = B.FORM_CODE ) t2 
            inner join EMP_MAS c1 on t2.LINE_TYPE = c1.LINE_TYPE and t2.POS_TYPE = c1.POS_TYPE 
             ) n1 ) n2 
            inner join EMP_MAS C on n2.EMP_CODE = C.EMP_CODE and n2.LINE_TYPE = c.LINE_TYPE and n2.POS_TYPE = c.POS_TYPE 
            left join DEVICE_APP DA on C.EMP_CODE = DA.EMP_CODE
            where NOF is null `;
      break;
  }

  let targetresult = await conn.ociExecute(target_sql, "", "");
  if (targetresult.results.lenght <= 0) {
    res.status(200).json({ message: "No data found." });
  }
  //convert target sql to send

  const mailtarget = targetresult.results.map((row) => row.EMAIL);
  const sendemailtarget = mailtarget.filter((item) => item !== null);
  const emailAddress = sendemailtarget.join(",");
  const linetoken = targetresult.results.map((row) => row.LINE_TOKEN);
  const LineAddress = linetoken
    .filter((value) => value !== null)
    .map((value) => value);
  const SMStarget = targetresult.results.map((row) => row.PHONE_NO);
  const sendSMStarget = SMStarget.filter((item) => item !== null);
  const SMSAddress = sendSMStarget.join(",");
  const fcmtarget = targetresult.results.map((row) => row.TOKEN);
  const FCMAddress = fcmtarget
    .filter((value) => value !== null)
    .map((value) => value);

  console.log(emailAddress);
  console.log(SMSAddress);

  //convertdate  date
  const [fromDay, fromMonth, fromYear] = fromDate.split("/");
  const FromDate = new Date(fromYear, fromMonth - 1, fromDay);
  const formattedfromDate = FromDate.toLocaleDateString("th-TH", options);
  const [endDay, endMonth, endYear] = endDate.split("/");
  const EndDate = new Date(endYear, endMonth - 1, endDay);
  const formattedendDate = EndDate.toLocaleDateString("th-TH", options);

  const Roundyear = parseInt(endYear, 10) + 543;
  //convert round date
  const [RendDay, RendMonth, RendYear] = RendDate.split("/");
  const Renddate = new Date(RendYear, RendMonth - 1, RendDay);
  const formattedRendDate = Renddate.toLocaleDateString("th-TH", options);

  const [RstartDay, RstartMonth, RstartYear] = RfromDate.split("/");
  const startday = new Date(RstartYear, RstartMonth - 1, RstartDay);
  const formattedRfromDate = startday.toLocaleDateString("th-TH", options);
  const today = new Date();
  const formattedDate = today.toLocaleDateString("th-TH", options);
  const diffInTime = Math.abs(startday.getTime() - Renddate.getTime());
  const diffInDays = Math.ceil(diffInTime / (1000 * 60 * 60 * 24));
  var mailsubject, message, subject, desc, followmessage, real_fromdate;
  console.log(option);
  if (today >= startday) {
    real_fromdate = formattedDate;
  } else {
    real_fromdate = formattedRfromDate;
  }

  followmessage = `ผลการปฏิบัติงาน ${roundName}\n ระหว่างวันที่ ${formattedfromDate}\n ถึงวันที่ ${formattedendDate} \nวันที่ประเมินได้จริง วันที่ ${real_fromdate} ถึงวันที่ ${formattedRendDate}\n`;
  switch (option) {
    case 1:
      mailsubject = `ปีงบประมาณ ${Roundyear} เปิด${roundName}`;
      message = `ปีงบประมาณ ${Roundyear} เปิด${roundName}\n ${followmessage}ระยะเวลาที่ประเมิน ${diffInDays} วัน`;
      break;
    case 2:
      mailsubject = `ปีงบประมาณ ${Roundyear} เปิด${roundName}`;
      message = `ปีงบประมาณ ${Roundyear} ปิด${roundName}\n ${followmessage}ระยะเวลาที่ประเมิน ${diffInDays} วัน`;
      break;
    case 3:
      mailsubject = `ปีงบประมาณ ${Roundyear} เปิด${roundName}`;
      message = `ปีงบประมาณ ${Roundyear} เปิด${roundName}\n ${followmessage} คุณยังทำการประเมินไม่สมบูรณ์ กรุณาเข้าไปทำการประเมิน`;
      break;
    case 4:
      mailsubject = `ปีงบประมาณ ${Roundyear} เปิด${roundName}`;
      message = `ปีงบประมาณ ${Roundyear} เปิด${roundName}\n ${followmessage} คุณยังไม่ได้ทำการประเมินผู้ใต้บังคับบัญชา กรุณาเข้าไปทำการประเมิน`;
      break;
  }
  subject = mailsubject;
  desc = message;
  console.log(mailsubject, "Message: " + message);

  sendNotify(
    emailAddress,
    LineAddress,
    SMSAddress,
    FCMAddress,
    pos_type,
    line_type,
    subject,
    desc,
    mailsubject,
    message
  );
}

//function to send notification
async function sendNotify(
  emailAddress,
  LineAddress,
  SMSAddress,
  FCMAddress,
  pos_type,
  line_type,
  subject,
  desc,
  mailsubject,
  message
) {
  check_noti = `SELECT NOTI_CHNL_TYPE, NOTI_SEND_YN
  FROM SYSTEM_NOTIFY`;
  let use_notification = await conn.ociExecute(check_noti, "", "");
  console.log(use_notification);
  const { results } = use_notification;
  const [rows, fields] = [results, null];
  console.log(rows);

  rows.forEach((row) => {
    const { NOTI_CHNL_TYPE, NOTI_SEND_YN } = row;

    switch (NOTI_CHNL_TYPE) {
      case "MAIL":
        if (NOTI_SEND_YN === "Y") {
          sendEmailNotification(
            emailAddress,
            (type = null),
            (subject = null),
            (desc = null),
            pos_type,
            line_type,
            mailsubject,
            message
          );
        }
        break;
      case "LINE":
        if (NOTI_SEND_YN === "Y") {
          sendLineNotification(
            LineAddress,
            (type = null),
            mailsubject,
            message,
            line_type,
            pos_type
          );
        }
        break;
      case "SMS":
        if (NOTI_SEND_YN === "Y") {
          sendSMSNotification(
            SMSAddress,
            message,
            mailsubject,
            message,
            line_type,
            pos_type
          );
        }
        break;
      case "MOBILE":
        if (NOTI_SEND_YN === "Y") {
          sendAppNotification(
            FCMAddress,
            mailsubject,
            message,
            line_type,
            pos_type
          );
        }
        break;
      default:
        // Do nothing for unknown notification types
        break;
    }
  });
}

module.exports = {
  optionnotify: optionnotify,
};
