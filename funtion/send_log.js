const conn = require("../config/db.js");
//make random string code
function generateRandomString(length) {
  let result = "";
  const characters =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}
//mail
async function sendNotifyLogmail(
  type,
  subject,
  desc,
  line_type,
  pos_type,
  ipaddress = "127.0.0.1"
) {
  const randomString = generateRandomString(10);
  const now = new Date();
  const dateStr = now.toLocaleDateString("th-TH", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  const currentDateString = now
    .toLocaleString("en-US", {
      timeZone: "UTC",
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    })
    .replace(/[^\d]/g, "");
  console.log(dateStr);
  logsql = `INSERT INTO LOG_SYSTEM_NOTIFY (
    EMP_CODE, NOTI_CHNL_TYPE, NOTI_CHNL_ADDR, SEND_DATE, SEND_CODE,
    SUBJECT, NOTE, NOTI_WARNING_DAY, CREATED_DATE, STATUS, LINE_TYPE, POS_TYPE
  ) SELECT
    EMP_CODE,
    '${type}',
    '${ipaddress}',
    '${dateStr}',
    '${currentDateString}',
    '${subject}',
    '${desc}',
    3,
    SYSDATE,
    'S',
    '${line_type}',
    '${pos_type}'
  FROM EMP_MAS 
    WHERE EMAIL IS NOT NULL
    AND POS_TYPE = '${pos_type}'
    AND LINE_TYPE = '${line_type}'
  `;
  console.log(logsql);
  await conn.ociCommandExecute(logsql, "", "");
}

//Line
async function sendNotifyLogline(
  type,
  subject,
  desc,
  line_type,
  pos_type,
  ipaddress = "127.0.0.1"
) {
  const randomString = generateRandomString(10);
  const now = new Date();
  const dateStr = now.toLocaleDateString("th-TH", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  const currentDateString = now
    .toLocaleString("en-US", {
      timeZone: "UTC",
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    })
    .replace(/[^\d]/g, "");
  console.log(dateStr);
  logsql = `INSERT INTO LOG_SYSTEM_NOTIFY (
    EMP_CODE, NOTI_CHNL_TYPE, NOTI_CHNL_ADDR, SEND_DATE, SEND_CODE,
    SUBJECT, NOTE, NOTI_WARNING_DAY, CREATED_DATE, STATUS, LINE_TYPE, POS_TYPE
  ) SELECT
    EMP_CODE,
    '${type}',
    '${ipaddress}',
    '${dateStr}',
    '${currentDateString}',
    '${subject}',
    '${desc}',
    3,
    SYSDATE,
    'S',
    '${line_type}',
    '${pos_type}'
  FROM EMP_MAS 
    WHERE EMAIL IS NOT NULL
    AND POS_TYPE = '${pos_type}'
    AND LINE_TYPE = '${line_type}'
  `;
  console.log(logsql);
  await conn.ociCommandExecute(logsql, "", "");
}
//app
async function sendNotifyLogapp(
  type,
  subject,
  desc,
  line_type,
  pos_type,
  ipaddress = "127.0.0.1"
) {
  const now = new Date();
  const dateStr = now.toLocaleDateString("th-TH", {
    year: "numeric",
    month: "2-digit",
    day: "2-digit",
  });
  const currentDateString = now
    .toLocaleString("en-US", {
      timeZone: "UTC",
      year: "numeric",
      month: "2-digit",
      day: "2-digit",
      hour: "2-digit",
      minute: "2-digit",
      second: "2-digit",
    })
    .replace(/[^\d]/g, "");
  console.log(dateStr);
  logsql = `INSERT INTO LOG_SYSTEM_NOTIFY (
    EMP_CODE, NOTI_CHNL_TYPE, NOTI_CHNL_ADDR, SEND_DATE, SEND_CODE,
    SUBJECT, NOTE, NOTI_WARNING_DAY, CREATED_DATE, STATUS, LINE_TYPE, POS_TYPE
  ) SELECT
    da.EMP_CODE,
    '${type}',
    '${ipaddress}',
    '${dateStr}',
    '${currentDateString}',
    '${subject}',
    '${desc}',
    3,
    SYSDATE,
    'S',
    '${line_type}',
    '${pos_type}'
  FROM EMP_MAS em
  INNER JOIN DEVICE_APP da ON em.EMP_CODE = da.EMP_CODE
    AND da.TOKEN IS NOT NULL
    AND em.POS_TYPE = '${pos_type}'
    AND em.LINE_TYPE = '${line_type}'
  `;
  console.log(logsql);
  await conn.ociCommandExecute(logsql, "", "");
}

module.exports = {
  sendNotifyLogmail: sendNotifyLogmail,
  sendNotifyLogline: sendNotifyLogline,
  sendNotifyLogapp: sendNotifyLogapp,
};
