const oracledb = require("oracledb");
var connectData = {
  user: "ASSESS",
  password: "asess13181924",
  //connectString : "119.59.101.250/ORCL"
  connectString: "27.254.173.102/ORCL",
  // connectString: "10.35.204.82/BDDB",
};

oracledb.autoCommit = true;

var oracledb_connection;
oracledb.getConnection(connectData, function (err, connection) {
  if (err) {
    console.error(err);
    return;
  }
  console.log("connected to orcale");
  oracledb_connection = connection;
});

// START : function execute database
function ociExecute(query, messageCode, messageError) {
  return new Promise(async function (resolve, reject) {
    // let conn; // Declared here for scoping purposes.
    let results = {};
    try {
      let result = await oracledb_connection.execute(query, [], {
        outFormat: oracledb.OBJECT,
      });

      // check return message info
      if (result.rows.length == 0) {
        if (messageError == "") {
          messageCode = "E01";
        } else {
          messageCode = messageError;
        }
      } else {
        if (messageCode == "") {
          messageCode = "S01";
        }
      }

      // let messageInfo = await getMessageInfo(messageCode);
      // console.log("messageInfo =>", messageInfo.rows);

      //console.log("executed.");
      // results["MESSAGE_CODE"] = messageInfo.rows[0]["MSG_STATUS"];
      // results["MESSAGE_TEXT_TH"] = messageInfo.rows[0]["MSG_DESC_TH"];
      // results["MESSAGE_TEXT_EN"] = messageInfo.rows[0]["MSG_DESC_EN"];
      results["results"] = result.rows;

      resolve(results);
    } catch (err) {
      console.log("Error occurred", err);

      reject(err);
    } finally {
      // If conn assignment worked, need to close.
      if (oracledb_connection) {
        try {
          // กรณีต้องการปิด การเชื่อมต่อ
          // await oracledb_connection.close();
          // console.log("Connection closed");
        } catch (err) {
          console.log("Error closing connection", err);
        }
      }
    }
  });
}
// END : function execute database

// START : function execute update & insert database *******************************************
function ociCommandExecute(query, messageCode) {
  return new Promise(async function (resolve, reject) {
    // let conn; // Declared here for scoping purposes.
    let results = {};
    try {
      let result = await oracledb_connection.execute(query, [], {
        outFormat: oracledb.OBJECT,
      });

      // let messageInfo = await getMessageInfo(messageCode);
      // console.log("messageInfo =>", messageInfo.rows);

      //console.log("executed insert & update.");
      //results["MESSAGE_CODE"] = messageInfo.rows[0]["MSG_STATUS"];
      //results["MESSAGE_TEXT_TH"] = messageInfo.rows[0]["MSG_DESC_TH"];
      //results["MESSAGE_TEXT_EN"] = messageInfo.rows[0]["MSG_DESC_EN"];

      resolve(results);
    } catch (err) {
      console.log("Error occurred", err);

      reject(err);
    } finally {
      // If conn assignment worked, need to close.
      if (oracledb_connection) {
        try {
          // กรณีต้องการปิด การเชื่อมต่อ
          // await oracledb_connection.close();
          // console.log("Connection closed");
        } catch (err) {
          console.log("Error closing connection", err);
        }
      }
    }
  });
}
// END : function execute update & insert database *******************************************

module.exports = { oracledb_connection, ociExecute, ociCommandExecute };
