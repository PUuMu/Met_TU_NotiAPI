require("dotenv").config();
const jwt = require("jsonwebtoken"); // ใช้งาน jwt module

// const fs = require("fs"); // ใช้งาน file system module ของ nodejs

// Default
// const verifyToken = ((req, res, next) => {
//   const bearerHeader = req.headers["authorization"];
//   if (typeof bearerHeader !== "undefined") {
//     const bearerToken = bearerHeader.split(" ")[1];
//     req.token = bearerToken;
//     next();
//   } else {
//     res.sendStatus(403); // Forbidden
//   }
// });

const authenticateToken = (req, res, next) => {
  const authHeader = req.headers["authorization"];
  const token = authHeader && authHeader.split(" ")[1];
  if (token == null) return res.sendStatus(401);

  jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
    if (err) {
      console.log("err :", err);
      return res.sendStatus(403);
    } else {
      req.user = user;
      next();
    }
  });
};

module.exports = authenticateToken; // ส่ง middleware ฟังก์ชั่นไปใช้งาน
