// const express = require("express");
// const jwt = require("jsonwebtoken");
// const fs = require('fs') // ใช้งาน file system module ของ nodejs
const path = require("path"); // เรียกใช้งาน path module
const schdule = require("./funtion/schedule.js"); //

var serveStatic = require("serve-static");
const bodyParser = require("body-parser");
// const app = express();

var app = require("express")();
var server = require("http").Server(app);
// var io = require("socket.io")(server);

const conn = require("./config/db.js");

//run schdulfuntion

// app.use(express.json())
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
// app.use(bodyParser.static(path.join(__dirname, 'public')))

app.use(
  bodyParser.json({
    limit: "50mb",
  })
);
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true,
  })
);

// set image virtual directory - server pixel
// app.use('/imgserver', serveStatic('C:\\imgserver\\'));

// set image virtual directory - localhost
app.use("/imgserver", serveStatic("D:\\imgserver\\"));

// ส่วนของการใช้งาน router module ต่างๆ
const authRouter = require("./authServer");
// const masterRouter = require('./routes/master')
// const roomRouter = require('./routes/room')
// const opentokRouter = require('./routes/opentok');
// const comechatRouter = require('./routes/comechat');

// const mainRouter = require('./routes/main');

// noti
const notiRouter = require("./routes/noti");
const tokenRouter = require("./routes/token_register"); //token register

const { Socket } = require("dgram");

// เรียกใช้งาน Router
app.use("/v1", authRouter);

// ส่ง Line Noti , Mail , SMS , Web Push
app.use("/v1", notiRouter);

// ส่ง token เช็ค token
app.use("/v1", tokenRouter);

// const PORT = 3009 // pixel
const PORT = 8003; // local
server.listen(PORT, (req, res) => {
  console.log("server started on port " + PORT);
});
