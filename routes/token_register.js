const jwt = require("jsonwebtoken"); // ใช้งาน jwt module
const fs = require("fs"); // ใช้งาน file system module ของ nodejs
const express = require("express");
const router = express.Router();
module.exports = router;
const verifyToken = require("../config/authorize.js");
const authenticateToken = verifyToken;
const conn = require("../config/db.js");
const formidable = require("formidable");
const mime = require("mime");
const notifications = require("./notifunction.js");
const axios = require("axios");

const bodyParser = require("body-parser");
const fireadmin = require("firebase-admin");
var serviceAccount = require("../config/push-notification-key.json"); //call firebase keyservice

const request = require("request");

//token register path
router.post("/token-register", async (req, res) => {
  const { empCode, fcmToken } = req.body;
  if (!fcmToken || !empCode) {
    res.status(400).json({ error: "fcmToken or emp code is required" });
    return;
  }

  try {
    console.log(fcmToken, empCode);

    const sql =
      `
    INSERT INTO DEVICE_APP (EMP_CODE, TOKEN, STATUS, DEVICE_NAME, REGISTER_DATE)
    VALUES ('` +
      empCode +
      `', '` +
      fcmToken +
      `', 'A', 'MOBILE', SYSDATE)
    `;
    console.log(sql);
    const bindParams = { fcmToken, empCode };

    const result = await conn.ociCommandExecute(sql, "", "");
    // const options = { outFormat: conn.oracledb.OUT_FORMAT_OBJECT };

    res.json({ message: "User Token registered successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

//dev path check fcm token
router.get("/token-check", async (req, res) => {
  try {
    const sql =
      "SELECT EMP_CODE, TOKEN FROM DEVICE_APP WHERE TOKEN IS NOT NULL";
    const result = await conn.ociExecute(sql, "", "");
    console.log(result);
    const tokenresult = result.results.map((row) => row.TOKEN);

    res.json(result.results);
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});
