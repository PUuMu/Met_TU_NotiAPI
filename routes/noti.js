const jwt = require("jsonwebtoken"); // ใช้งาน jwt module
const fs = require("fs"); // ใช้งาน file system module ของ nodejs
const express = require("express");
const router = express.Router();
module.exports = router;
const verifyToken = require("../config/authorize.js");
const authenticateToken = verifyToken;
const conn = require("../config/db.js");
const formidable = require("formidable");
const logNotify = require("../funtion/send_log.js");
const mime = require("mime");
const notifications = require("./notifunction.js");
const axios = require("axios");
const bodyParser = require("body-parser");
const { body, validationResult } = require("express-validator");
const optionnotify = require("../funtion/optionnotiry.js");

const schedule = require("../funtion/schedule.js");
var serviceAccount = require("../config/push-notification-key.json"); //call firebase keyservice
require("dotenv").config();

const thaibulksmsApi = require("thaibulksms-api");

const request = require("request");
//use bodyParser
router.use(bodyParser.json());

//sms_api
const options = {
  apiKey: process.env.SMS_API_KEY,
  apiSecret: process.env.SMS_API_SECRET,
};
const sms = thaibulksmsApi.sms(options);

//line notification path
router.post("/send-line", authenticateToken, async (req, res) => {
  const axios = require("axios");
  const {
    emp_code,
    type = null,
    subject,
    desc,
    pos_type,
    line_type,
    ipaddress,
  } = req.body;
  if (!emp_code & !pos_type & !line_type) {
    res.status(400).json({ error: "Reciver is empty" });
    return;
  }
  try {
    // create send target
    let targetsql =
      "SELECT LINE_TOKEN FROM EMP_MAS WHERE LINE_TOKEN IS NOT NULL";
    if (emp_code) {
      targetsql += ` AND EMP_CODE = '${emp_code}'`;
    }
    if (pos_type) {
      targetsql += ` AND POS_TYPE = '${pos_type}'`;
    }
    if (line_type) {
      targetsql += ` AND LINE_TYPE = '${line_type}'`;
    }
    console.log(targetsql);

    //get target
    let targetresult = await conn.ociExecute(targetsql, "", "");
    console.log("targetresult", targetresult.results);

    // return;
    // const targetresult = await connection.execute(targetsql, [], options);
    //data to send

    if (targetresult.results.lenght <= 0) {
      res.status(200).json({ message: "No data found." });
    }

    const target_token = targetresult.results.map((row) => row.LINE_TOKEN);

    //sendlinenotification
    notifications.sendLineNotification(
      target_token,
      type,
      subject,
      desc,
      line_type,
      pos_type,
      ipaddress
    );

    res.json({ message: "Line notification sent successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

//mail notification path
router.post("/send-mail", authenticateToken, async (req, res) => {
  const {
    emp_code,
    type = null,
    subject,
    desc,
    pos_type,
    line_type,
    ipaddress,
  } = req.body;
  if (!emp_code & !pos_type & !line_type) {
    res.status(400).json({ error: "Reciver is empty" });
    return;
  }
  try {
    // create send target
    let targetsql = "SELECT EMAIL FROM EMP_MAS WHERE EMAIL IS NOT NULL";
    if (emp_code) {
      targetsql += ` AND EMP_CODE = '${emp_code}'`;
    }
    if (pos_type) {
      targetsql += ` AND POS_TYPE = '${pos_type}'`;
    }
    if (line_type) {
      targetsql += ` AND LINE_TYPE = '${line_type}'`;
    }
    console.log(targetsql);
    //get target result
    let targetresult = await conn.ociExecute(targetsql, "", "");
    console.log("targetresult", targetresult.results);
    if (targetresult.results.lenght <= 0) {
      res.status(200).json({ message: "No data found." });
    }
    console.log(pos_type, line_type);

    const target = targetresult.results.map((row) => row.EMAIL);
    //sendlinenotification
    await notifications.sendEmailNotification(
      target,
      type,
      subject,
      desc,
      pos_type,
      line_type,
      ipaddress
    );

    res.json({ message: "email sent successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

//SMS notification path
router.post(
  "/send-sms",

  body("phone_number").isMobilePhone("th-TH"),
  body("message").not().isEmpty(),
  body("subject"),
  body("line_type"),
  body("pos_type"),
  body("ipaddress"),

  async (req, res) => {
    console.log("req.body is ", req.body);
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      return res.status(400).json({ errors: errors.array() });
    }

    try {
      let phoneNumber = req.body.phone_number;
      console.log("phoneNumber", phoneNumber);
      let message = req.body.message;
      let body = {
        msisdn: phoneNumber,
        message: message,
        // sender: "MED_TU_NOTI",
        // scheduled_delivery: '',
        // force: ''
      };
      console.log(body);
      const response = await sms.sendSMS(body);

      //set log massage
      //set massege
      var mailsubject, maildesc, line_type, pos_type, ipaddress;

      mailsubject = req.body.subject;
      maildesc = req.body.message;
      line_type = req.body.line_type;
      pos_type = req.body.pos_type;
      ipaddress = req.body.ipaddress;

      //sendlog
      await logNotify.sendNotifyLogline(
        "SMS",
        mailsubject,
        maildesc,
        line_type,
        pos_type,
        ipaddress
      );
      // res.json(response.data)
      res.status(200).json({ success: "success" });
    } catch (error) {
      return res.status(500).json({ errors: error });
    }
  }
);

router.post("/send-app", authenticateToken, async (req, res) => {
  const {
    emp_code,
    type = null,
    subject,
    desc,
    pos_type,
    line_type,
    ipaddress,
  } = req.body;
  if (!emp_code & !pos_type & !line_type) {
    res.status(400).json({ error: "Reciver is empty" });
    return;
  }
  try {
    //target sql
    let targetsql = `SELECT da.EMP_CODE, da.TOKEN
  FROM EMP_MAS em
  INNER JOIN DEVICE_APP da ON em.EMP_CODE = da.EMP_CODE
  WHERE da.TOKEN IS NOT NULL`;

    // Add optional conditions to the SQL query
    if (pos_type) {
      targetsql += ` AND POS_TYPE = ${pos_type}`;
    }

    if (line_type) {
      targetsql += ` AND LINE_TYPE = ${line_type}`;
    }

    let targetresult = await conn.ociExecute(targetsql, "", "");
    console.log("targetresult", targetresult.results);
    if (targetresult.results.length <= 0) {
      return { message: "No data found." };
    }
    const emp_target = targetresult.results.map((row) => row.EMP_CODE);
    const fcmtarget = targetresult.results.map((row) => row.TOKEN);
    notifications.sendAppNotification(
      fcmtarget,
      subject,
      desc,
      line_type,
      pos_type,
      ipaddress
    );

    res.json({ message: "Notification sent successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/send-schdule", async (req, res) => {
  const {
    emp_code,
    current_round_code,
    subject,
    desc,
    pos_type,
    line_type,
    from_date,
    end_date,
    warning,
    lastwarning,
    ipaddress,
  } = req.body;
  if (!emp_code & !pos_type & !line_type) {
    res.status(400).json({ error: "Reciver is empty" });
    return;
  }
  try {
    console.log(
      current_round_code,
      line_type,
      pos_type,
      subject,
      from_date,
      end_date,
      warning,
      lastwarning
    );
    await schedule.sendNotification(
      current_round_code,
      line_type,
      pos_type,
      subject,
      from_date,
      end_date,
      warning,
      lastwarning
    );
    res.json({ message: "schdule sent successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/send-schdule-test", async (req, res) => {
  try {
    await schedule.schedulenotify();
    res.json({ message: "schdule sent successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});

router.post("/send-opt-notify", authenticateToken, async (req, res) => {
  const { option } = req.body;
  if (!option) {
    res.status(400).json({ error: "Reciver is empty" });
    return;
  }
  try {
    await optionnotify.optionnotify(option);
    res.json({ message: "Notification sent successfully" });
  } catch (err) {
    console.error(err);
    res.status(500).json({ error: "Internal server error" });
  }
});
