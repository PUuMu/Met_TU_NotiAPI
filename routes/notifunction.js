require("dotenv").config();
const nodemailer = require("nodemailer");
const thaibulksmsApi = require("thaibulksms-api");
// const twilio = require("twilio");
const conn = require("../config/db.js");
const axios = require("axios");
const logNotify = require("../funtion/send_log.js");
const bodyParser = require("body-parser");
const admin = require("firebase-admin");
var serviceAccount = require("../config/push-notification-key.json"); //call firebase keyservice
const express = require("express");
const router = express.Router();

//setfirbase

//setup firebase
admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

//use bodyParser
router.use(bodyParser.json());
//sms_api
const options = {
  apiKey: process.env.SMS_API_KEY,
  apiSecret: process.env.SMS_API_SECRET,
};
const sms = thaibulksmsApi.sms(options);

//setup smtp or mail service
let transporter = nodemailer.createTransport({
  host: "smtp.gmail.com",
  port: 465,
  secure: true,
  logger: true,
  debug: true,

  auth: {
    user: "poomchaiprasop@gmail.com",
    pass: "mezohdbtwswlkena",
  },
});

// Function to send email notification
async function sendEmailNotification(
  target,
  type = null,
  subject = null,
  desc = null,
  pos_type,
  line_type,
  mailsubject,
  maildescript,
  ipaddress
) {
  console.log(mailsubject, maildescript);

  // sql check type -> get data message
  let sql =
    "SELECT MSG_DESC_TH, MSG_DESC_EN, MSG_TITLE_TH, MSG_TITLE_EN, MSG_SH_YN FROM MESSAGE_INFO";
  if (type) {
    sql += ` WHERE MSG_TYPE = '${type}'`;
  } else if (subject) {
    sql += ` WHERE MSG_TITLE_TH = '${subject}' OR MSG_TITLE_EN = '${subject}' `;
  } else if (desc) {
    sql += ` WHERE MSG_DESC_TH = '${desc}' OR MSG_DESC_EN = '${desc}'`;
  }

  // DATA MESSAGE
  let result = await conn.ociExecute(sql, "", "");

  // return;
  // const targetresult = await connection.execute(targetsql, [], options);
  //data to send
  // if (type !== null) {
  //   const SH_YN = result.results.map((row) => row.MSG_SH_YN).join("");
  //   console.log("SH_YN", SH_YN);

  //   if (SH_YN === "TH") {
  //     mailsubject = result.results.map((row) => row.MSG_TITLE_TH).join("");
  //     maildescript = result.results.map((row) => row.MSG_DESC_TH).join("");
  //   } else {
  //     mailsubject = result.results.map((row) => row.MSG_TITLE_EN).join("");
  //     maildescript = result.results.map((row) => row.MSG_DESC_EN).join("");
  //   }
  // }
  if (subject !== null && desc !== null) {
    mailsubject = subject;
    maildescript = desc;
  }

  // setup email data
  let mailOptions = {
    from: "ralgrad_t@hotmail.com", // sender address
    to: target, // list of receivers
    subject: mailsubject, // email subject
    text: maildescript, // plain text body
  };

  // send email
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      console.log(error);
    } else {
      console.log("Email sent: " + info.response);
      res.json({ message: "email sent successfully" });
    }
  });
  logNotify.sendNotifyLogmail(
    "MAIL",
    mailsubject,
    maildescript,
    line_type,
    pos_type,
    ipaddress
  );
}

// Function to send SMS notification
async function sendSMSNotification(
  phoneNumber,
  message,
  subject,
  desc,
  line_type,
  pos_type,
  ipaddress
) {
  const url = "http://localhost:8003/v1/send-sms";
  const data = {
    phone_number: phoneNumber,
    message: message,
    subject: subject,
    line_type: line_type,
    pos_type: pos_type,
    ipaddress: ipaddress,
  };
  const headers = {
    "Content-Type": "application/json",
  };

  try {
    const response = await axios.post(url, data, { headers });
    console.log(response.data);
  } catch (error) {
    console.error(error);
  }
}

// Function to send LINE notification
async function sendLineNotification(
  target_token,
  type = null,
  subject,
  desc,
  line_type,
  pos_type,
  ipaddress
) {
  //set massege
  var mailsubject, maildesc;

  mailsubject = subject;
  maildesc = desc;

  //loop throung target tokens
  for (let i = 0; i in target_token; i++) {
    console.log(i);
    const token = target_token[i];
    console.log(token);

    // set message
    //const message = maildesc;

    // set LINE Notify API endpoint
    const url = "https://notify-api.line.me/api/notify";

    // set HTTP headers
    const headers = {
      "Content-Type": "application/x-www-form-urlencoded",
      Authorization: `Bearer ${token}`,
    };

    // set HTTP request body
    const data = new URLSearchParams();
    data.append("message", maildesc);

    // send LINE notification
    axios
      .post(url, data, { headers })
      .then((res1) => {
        console.log(`LINE notification sent: ${res1.data.message}`);
        res.status(200).json({ message: "Send message success." });
      })
      .catch((error) => {
        console.error(error);
      });
  }
  logNotify.sendNotifyLogline(
    "LINE",
    mailsubject,
    maildesc,
    line_type,
    pos_type,
    ipaddress
  );
}

//funtiontosend app noti
async function sendAppNotification(
  target,
  subject,
  desc,
  line_type,
  pos_type,
  ipaddress
) {
  if (target.length === 0) {
    return { message: "No targets to send notification to." };
  }

  const message = {
    notification: {
      subject,
      desc,
    },
    priority: "high",
    tokens: target,
  };

  const response = await admin.messaging().sendMulticast(message);
  logNotify.sendNotifyLogapp(
    "MOBILE",
    subject,
    desc,
    line_type,
    pos_type,
    ipaddress
  );

  return { message: "Notification sent." };
}

module.exports = {
  sendEmailNotification: sendEmailNotification,
  sendSMSNotification: sendSMSNotification,
  sendLineNotification: sendLineNotification,
  sendAppNotification: sendAppNotification,
};
