require("dotenv").config();

const conn = require("./config/db.js");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const express = require("express");

const request = require("request");

const router = express.Router();
module.exports = router;

let refreshTokens = [];

router.post("/token", (req, res) => {
  // console.log("token",req)
  const refreshToken = req.body.refreshToken;
  if (refreshToken == null) return res.sendStatus(401);
  if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403);
  jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
    if (err) return res.sendStatus(403);
    const accessToken = generateAccessToken({ name: user.name });
    res.json({ status: "success", content: { accessToken: accessToken } });
  });
});

router.delete("/logout", (req, res) => {
  refreshTokens = refreshTokens.filter((token) => token !== req.body.token);
  res.sendStatus(200);
});

router.post("/login", async (req, res) => {
  // Authenticate User
  const username = req.body.username;
  // const idcard = req.body.password;
  const password = req.body.password;

  const deviceName = req.body.deviceName;
  const deivceSoftwareVersion = req.body.deivceSoftwareVersion;
  const deviceIp = req.body.deviceIp;
  const deviceUuid = req.body.deviceUuid;
  const deviceModelName = req.body.deviceModelName;
  const deviceModelNumber = req.body.deviceModelNumber;
  const deviceSerialNumber = req.body.deviceSerialNumber;
  // const appName = req.body.appName;
  const user = { name: username };
  try {
    var currentdate = new Date();
    let time =
      String(currentdate.getMinutes()).padStart(2, "0") +
      "" +
      String(currentdate.getSeconds()).padStart(2, "0") +
      "" +
      currentdate.getMilliseconds();

    var dd = String(currentdate.getDate()).padStart(2, "0");
    var mm = String(currentdate.getMonth() + 1).padStart(2, "0");
    var yyyy = currentdate.getFullYear();
    var sessionId = dd + "" + mm + "" + yyyy + time;

    let query =
      `select A.* 
    from EMP_MAS A
    where EMP_CODE = '` +
      username +
      `' AND EMP_CODE IS NOT NULL `;
    let data = await conn.ociExecute(query, "", "");
    console.log(query);
    console.log(data.results);

    if (data.results.length > 0) {
      if (data.results[0]["EMP_PWD"] != password) {
        res.status(400).json({ error: "Invalid password." });
        return;
      }

      const accessToken = generateAccessToken(user);
          const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
          refreshTokens.push(refreshToken);
          res.json({
            status: "success",
            content: {
              empCode: data.results[0]["EMP_CODE"],
              // empPrefix: data.results[0]["EMP_PREFIX"],
              empFname: data.results[0]["FNAME"],
              empLname: data.results[0]["LNAME"],
              // empId: data.results[0]["EMP_ID"],
              // empDesc: data.results[0]["EMP_DESC"],
              sessionId: sessionId,
              accessToken: accessToken,
              refreshToken: refreshToken,
            },
          });



      // var uid = data.results[0]["WARD_CODE"];
      // var options = {
      //   method: "GET",
      //   url:
      //     "https://" +
      //     process.env.APP_ID +
      //     ".api-us.cometchat.io/v3/users/" +
      //     uid,
      //   headers: {
      //     apiKey: process.env.API_KEY_CC,
      //     onBehalfOf: uid,
      //   },
      // };

      // request(options, async function (error, response, body) {
      //   //if (error) throw new Error(error);

      //   console.log(body);
      //   var uid_ = JSON.parse(body);

      //   // console.log(uid_);123
      //   if (uid_.data !== undefined) {
      //     // console.log(uid_.data.uid);

      //     query =
      //       `INSERT INTO TELECONF_LOGIN_LOG (USER_LOGIN,LOGIN_DATE_TIME,LOGIN_TYPE,DEVICE_NAME
      //     ,DEVICE_SOFTWARE_VERSION,DEVICE_MODEL_NAME,DEVICE_MODEL_NUMBER,DEVICE_IP,DEVICE_UUID
      //     ,DEVICE_SERIAL_NUMBER,SESSION_ID,APP_NAME) 
      //     VALUES ( '` +
      //       username +
      //       `', SYSDATE ,'0','` +
      //       deviceName +
      //       `','` +
      //       deivceSoftwareVersion +
      //       `','` +
      //       deviceModelName +
      //       `','` +
      //       deviceModelNumber +
      //       `','` +
      //       deviceIp +
      //       `','` +
      //       deviceUuid +
      //       `','` +
      //       deviceSerialNumber +
      //       `','` +
      //       sessionId +
      //       `','` +
      //       appName +
      //       `')  `;
      //     // console.log("query",query);
      //     await conn.ociCommandExecute(query, "");

      //     // if (bcrypt.compareSync(password, data.results[0]["PWD"])) {
      //     const accessToken = generateAccessToken(user);
      //     const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
      //     refreshTokens.push(refreshToken);
      //     res.json({
      //       status: "success",
      //       content: {
      //         wardCode: data.results[0]["WARD_CODE"],
      //         wardName: data.results[0]["WARD_NAME"],
      //         buildName: data.results[0]["BUILD_NAME"],
      //         sessionId: sessionId,
      //         accessToken: accessToken,
      //         refreshToken: refreshToken,
      //       },
      //     });
      //   } else {
      //     var url_ =
      //       "https://" + process.env.APP_ID + ".api-us.cometchat.io/v3/users";
      //     var options = {
      //       method: "POST",
      //       url: url_,
      //       headers: {
      //         apiKey: process.env.API_KEY_CC,
      //       },
      //       body: {
      //         uid: data.results[0]["WARD_CODE"],
      //         name: data.results[0]["WARD_NAME"],
      //         // link: "-",
      //         role: "default",
      //         // metadata: "-",
      //         // withAuthToken: "-",
      //         // tags: ["-"],
      //       },
      //       json: true,
      //     };

      //     request(options, async function (error, response, body) {
      //       if (error) {
      //         //throw new Error(error);
      //         //res.status(400).json(error);
      //       }

      //       query =
      //         `INSERT INTO TELECONF_LOGIN_LOG (USER_LOGIN,LOGIN_DATE_TIME,LOGIN_TYPE,DEVICE_NAME
      //       ,DEVICE_SOFTWARE_VERSION,DEVICE_MODEL_NAME,DEVICE_MODEL_NUMBER,DEVICE_IP,DEVICE_UUID
      //       ,DEVICE_SERIAL_NUMBER,SESSION_ID,APP_NAME) 
      //       VALUES ( '` +
      //         username +
      //         `', SYSDATE ,'0','` +
      //         deviceName +
      //         `','` +
      //         deivceSoftwareVersion +
      //         `','` +
      //         deviceModelName +
      //         `','` +
      //         deviceModelNumber +
      //         `','` +
      //         deviceIp +
      //         `','` +
      //         deviceUuid +
      //         `','` +
      //         deviceSerialNumber +
      //         `','` +
      //         sessionId +
      //         `','` +
      //         appName +
      //         `')  `;
      //       // console.log("query",query);
      //       await conn.ociCommandExecute(query, "");

      //       // if (bcrypt.compareSync(password, data.results[0]["PWD"])) {
      //       const accessToken = generateAccessToken(user);
      //       const refreshToken = jwt.sign(
      //         user,
      //         process.env.REFRESH_TOKEN_SECRET
      //       );
      //       refreshTokens.push(refreshToken);
      //       res.json({
      //         status: "success",
      //         content: {
      //           wardCode: data.results[0]["WARD_CODE"],
      //           wardName: data.results[0]["WARD_NAME"],
      //           buildName: data.results[0]["BUILD_NAME"],
      //           sessionId: sessionId,
      //           accessToken: accessToken,
      //           refreshToken: refreshToken,
      //         },
      //       });
      //       // console.log(body);
      //       // res.status(400).json(body);
      //     });
      //   }
      // });

      // console.log("arr", arr);
      // res.json({ status: 200, results: arr });
      // }
      // else{
      // res.status(400).json({ error: "Invalid password." });
      // }
    } else {
      // res.sendStatus(400)
      // res.json({ status: "failed", content: [] });
      res.status(400).json({ error: "Invalid user or password." });
    }
  } catch (e) {
    console.log(e);
    //arr.push({ error: "plase check database." });
    //res.json({ status: 501, results: arr });
    // res.sendStatus(204);
    res.status(400).json({ error: "plase check database." });
  }
});

router.post("/patient_login", async (req, res) => {
  // Authenticate User
  var arr = Array();
  const wardCode = req.body.wardCode;
  const room = req.body.room;
  const user = { name: room };
  try {
    const accessToken = generateAccessToken(user);
    const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET);
    refreshTokens.push(refreshToken);

    let query = "";
    query =
      ` select B.* , (select A.PREFIX||A.FNAME||' '||A.LNAME NAME from TELECONF_PATIENT_INFO A where A.HN = B.HN AND ROWNUM = 1 ) NAME
    , ROWNUM as NN , (select WARD_NAME from TELECONF_WARD_MASTER C where B.WARD_CODE = C.WARD_CODE and ROWNUM = 1 )WARD_NAME
    , (select PATIENT_PHOTO from TELECONF_PATIENT_INFO A where A.HN = B.HN AND ROWNUM = 1 ) PATIENT_PHOTO
    , (select BUILD_NAME from TELECONF_WARD_MASTER C 
    left outer join TELECONF_BUILDING D on C.BUILD_CODE = D.BUILD_CODE
    where B.WARD_CODE = C.WARD_CODE and ROWNUM = 1 )BUILD_NAME
    from TELECONF_ROOM_SETTING B where B.WARD_CODE = '` +
      wardCode +
      `' AND B.ROOM = '` +
      room +
      `' AND B.STATUS = 'A' order by created_date asc `;

    console.log(query);
    let data = await conn.ociExecute(query, "", "");
    if (data.results.length > 0) {
      for (i = 0; i < data.results.length; i++) {
        arr.push({
          hn: data.results[i]["HN"],
          wardCode: data.results[i]["WARD_CODE"],
          room: data.results[i]["ROOM"],
          name: data.results[i]["NAME"],
          photo: data.results[i]["PATIENT_PHOTO"]
            ? data.results[i]["PATIENT_PHOTO"].replace(
                "localhost:3000",
                "192.168.1.34:3000"
              )
            : "",
          wardName: data.results[i]["WARD_NAME"],
          buildName: data.results[i]["BUILD_NAME"],
          apiSessionId: data.results[i]["API_SESSION_ID"],
          apiToken: data.results[i]["API_TOKEN"],
          accessToken: accessToken,
          refreshToken: refreshToken,
        });
      }

      // console.log("arr", arr);
      res.json({ status: "success", content: arr });
    } else {
      // res.sendStatus(400);
      res.status(400).json({ error: "No data found." });
    }
  } catch (e) {
    console.log(e);
    //arr.push({ error: "plase check database." });
    //res.json({ status: 501, results: arr });
    // res.sendStatus(204);
    res.status(400).json({ error: "plase check database." });
  }
});

function generateAccessToken(user) {
  return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: "1m" });
}
