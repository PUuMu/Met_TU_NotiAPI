//sms_api
require("dotenv").config();

const thaibulksmsApi = require("thaibulksms-api");

const options = {
  apiKey: process.env.SMS_API_KEY,
  apiSecret: process.env.SMS_API_SECRET,
};
const sms = thaibulksmsApi.sms(options);

// Function to send SMS notification
async function sendSMSNotification(phone_number, message) {
  try {
    let phoneNumber = phone_number;
    let body = {
      msisdn: phone_number,
      message: message,
      // sender: "MED_TU",
    };
    const response = await sms.sendSMS(body);
    return response.data;
  } catch (error) {
    throw error;
  }
}

sendSMSNotification("0841750062", "message")
  .then((response) => {
    console.log(response);
  })
  .catch((error) => {
    console.error(error);
  });
